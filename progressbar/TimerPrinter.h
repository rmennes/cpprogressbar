//
// Created by Ruben Mennes on 23/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_TIMERPRINTER_H_
#define LIBPROGRESSBAR_SRC_TIMERPRINTER_H_

#include "progressbar_types.h"
#include <ostream>

namespace progressbar {

class TimerPrinter {
 public:
  void operator()(TimeDurationMs duration, std::ostream& stream) const;
};

}

#endif //LIBPROGRESSBAR_SRC_TIMERPRINTER_H_
