//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_INCLUDE_RATIO_H_
#define LIBPROGRESSBAR_SRC_INCLUDE_RATIO_H_

#include <iostream>

namespace progressbar{

class Ratio{
 public:
  Ratio(double p = 0.0);

  operator double() const {return m_p;}

  Ratio operator+(double) const;
  Ratio operator+(const Ratio&) const;
  Ratio operator-(double) const;
  Ratio operator-(const Ratio&) const;
  Ratio operator*(double) const;
  Ratio operator*(const Ratio&) const;
  Ratio operator/(double) const;
  Ratio operator/(const Ratio&) const;

  Ratio& operator+=(double);
  Ratio& operator+=(const Ratio&);
  Ratio& operator-=(double);
  Ratio& operator-=(const Ratio&);
  Ratio& operator*=(double);
  Ratio& operator*=(const Ratio&);
  Ratio& operator/=(double);
  Ratio& operator/=(const Ratio&);

  friend std::ostream& operator<<(std::ostream&, const Ratio& r);

 private:
  static double limit_value(double p);
  double m_p;
};

}

#endif //LIBPROGRESSBAR_SRC_INCLUDE_RATIO_H_
