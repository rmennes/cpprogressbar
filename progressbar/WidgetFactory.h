//
// Created by Ruben Mennes on 23/04/2020.
//

#ifndef LIBPROGRESSBAR_PROGRESSBAR_WIDGETFACTORY_H_
#define LIBPROGRESSBAR_PROGRESSBAR_WIDGETFACTORY_H_

#include "widgets/AbstractWidget.h"
#include <memory>
#include <vector>

namespace progressbar {

class ProgressBar;

class WidgetFactory {
 public:
  friend class ProgressBar;
  typedef std::unique_ptr<widgets::AbstractWidget> AbstractWidgetPtr;

  template<class Widget, class... Args>
  void create_widget(Args... args);
  void add_widget(AbstractWidgetPtr&&);

  std::size_t size() const {return m_widgets.size();}

 private:
  std::vector<AbstractWidgetPtr>  m_widgets;
};

template<class Widget, class... Args>
void WidgetFactory::create_widget(Args... args) {
  AbstractWidgetPtr widget = std::make_unique<Widget>(args...);
  m_widgets.push_back(std::move(widget));
}

}

#endif //LIBPROGRESSBAR_PROGRESSBAR_WIDGETFACTORY_H_
