//
// Created by Ruben Mennes on 23/04/2020.
//

#include "TimerPrinter.h"
#include <iomanip>

namespace progressbar{

void TimerPrinter::operator()(TimeDurationMs duration_ms, std::ostream &stream) const {
  double duration_secs = (double)duration_ms / 1000.0;
  unsigned int duration_minutes = duration_secs / 60;
  double duration_secs_to_display = duration_secs - (duration_minutes * 60);
  unsigned int duration_hours = duration_minutes / 60;
  unsigned int duration_days = duration_hours / 24;

  if(duration_days > 0 || duration_hours > 23){
    stream << duration_days << " days ";
  }
  stream << std::setw(2) << std::setfill('0') << duration_hours % 24 << ':' <<
         std::setw(2) << std::setfill('0') << duration_minutes % 60 << ':' << std::fixed <<
         std::setw(6) << std::setfill('0') << std::setprecision(3) <<
         duration_secs_to_display;
}

}