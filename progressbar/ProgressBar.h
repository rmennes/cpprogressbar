//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_PROGRESSBAR_H_
#define LIBPROGRESSBAR_SRC_PROGRESSBAR_H_

#include <memory>
#include <vector>
#include <ostream>
#include <sstream>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include "ProgressStatus.h"
#include "WidgetFactory.h"
#include "widgets/AbstractWidget.h"
#include "widgets/VariableLengthWidget.h"
#include "widgets/FixedLengthWidget.h"
#include "config.h"

namespace progressbar {

class ProgressBar {
 public:
  typedef std::unique_ptr<widgets::AbstractWidget> AbstractWidgetPtr;

  static std::string version() {return libprogressbar_VERSION_STR;}

  ProgressBar(WidgetFactory&, std::ostream& stream = std::cout);
  ProgressBar(Step end, WidgetFactory&, std::ostream& stream = std::cout);
//  ProgressBar(Step start, Step end, WidgetFactory&&, std::ostream& stream = std::cout);
  ~ProgressBar();

  void push_back_widget(AbstractWidgetPtr&& widget);

  template<class Widget, class... Args>
  void add_widget(Args...);

  void enable();
  void disable();

  void set_max_steps(Step max);
  void set_time_delay_ms(unsigned int time = 250);

  const ProgressStatus& get_status() const {return m_status;}

  ProgressBar& operator+=(const Step&);
  ProgressBar& operator++();
  operator bool() const;

 private:
  class DisplayElement{
   public:
    friend class ProgressBar;

    DisplayElement(const AbstractWidgetPtr&);

    inline bool is_set() const {return m_set;}
    SymbolLength size() const {return m_ss.str().length();}

    const AbstractWidgetPtr&  m_widget;
    bool                      m_set;
    std::stringstream         m_ss;
  };

  void start();
  void stop();

  void display_loop();
  SymbolLength get_window_size() const;
//  void check_time_and_display();
  void display();

  std::atomic_bool                                      m_start;
  std::atomic_bool                                      m_enabled;
  std::ostream&                                         m_stream;
  ProgressStatus                                        m_status;
  std::vector<AbstractWidgetPtr>                        m_widgets;
  Scale                                                 m_total_scale;
  std::atomic_uint32_t                                  m_ms_delay;
  Ratio                                                 m_last_ratio;
  TimePoint                                             m_last_display_time;

  std::thread                                           m_display_thread;
  mutable std::mutex                                    m_display_mutex;
  mutable std::condition_variable                       m_display_cv;
  std::atomic_bool                                      m_display;
  std::atomic_bool                                      m_display_run;
};

template<class Widget, class... Args>
void ProgressBar::add_widget(Args... args) {
  auto widget = std::make_unique<Widget>(args...);
  push_back_widget(std::move(widget));
}

}

#endif //LIBPROGRESSBAR_SRC_PROGRESSBAR_H_
