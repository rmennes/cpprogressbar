//
// Created by Ruben Mennes on 22/04/2020.
//

#include "Timer.h"
#include <iomanip>

namespace progressbar{
namespace widgets{

Timer::Timer(): FixedLengthWidget() {

}

Timer::~Timer() noexcept {

}

void Timer::stream_to(std::ostream &stream) const {
  TimeDurationMs duration_ms = m_status->duration_ms();
  m_printer(duration_ms, stream);
}

}
}