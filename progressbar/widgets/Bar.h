//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_BAR_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_BAR_H_

#include "VariableLengthWidget.h"
#include <vector>

namespace progressbar {
namespace widgets {

class Bar: public VariableLengthWidget {
 public:
  Bar(Scale scale = 1.0,
      char complete_char = '=', char incomplete_char = ' ');
  virtual ~Bar();

  void stream_to(std::ostream& stream, SymbolLength length) const;

  Bar& set_complete_char(char complete_char);
  Bar& set_incomplete_char(char incomplete_char);
  inline char get_complete_char() const
    {return m_complete_char;}
  inline char get_incomplete_char() const
    {return m_incomplete_char;}

 private:
  char rotating_symbol() const;

  char                    m_complete_char;
  char                    m_incomplete_char;
  mutable unsigned int    m_rotating_step;
  static const std::vector<char> m_rotating_symbol;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_BAR_H_
