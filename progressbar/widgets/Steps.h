//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_STEPS_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_STEPS_H_

#include "FixedLengthWidget.h"

namespace progressbar {
namespace widgets {

class Steps: public FixedLengthWidget {
 public:
  Steps();
  virtual ~Steps();

  void stream_to(std::ostream& stream) const;
};

}
}
#endif //LIBPROGRESSBAR_SRC_WIDGETS_STEPS_H_
