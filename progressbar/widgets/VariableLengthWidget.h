//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_VARIABLELENGTHWIDGET_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_VARIABLELENGTHWIDGET_H_

#include "AbstractWidget.h"

namespace progressbar {
namespace widgets {

class VariableLengthWidget : public AbstractWidget {
 public:
  VariableLengthWidget(Scale scale = 1.0);
  virtual ~VariableLengthWidget();

  bool is_fixed() const {return false;}
  void calculate_length(SymbolLength total, SymbolLength allocated, Scale scale_total);
  void stream_to(std::ostream& stream) const;
  Maybe<Scale> get_variable_scale() const {return Maybe<Scale>(m_scale);}
  virtual void stream_to(std::ostream& stream, SymbolLength length) const = 0;

  SymbolLength get_symbols_to_use() const { return m_symbols_to_use; }

 private:
  Scale m_scale;
  SymbolLength m_symbols_to_use;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_VARIABLELENGTHWIDGET_H_
