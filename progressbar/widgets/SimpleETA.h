//
// Created by Ruben Mennes on 23/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_SIMPLEETA_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_SIMPLEETA_H_

#include "FixedLengthWidget.h"
#include "../TimerPrinter.h"

namespace progressbar {
namespace widgets {

class SimpleETA: public FixedLengthWidget {
 public:
  SimpleETA();
  virtual ~SimpleETA() noexcept ;

  void stream_to(std::ostream &stream) const;
 private:
  TimerPrinter      m_printer;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_SIMPLEETA_H_
