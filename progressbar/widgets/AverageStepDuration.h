//
// Created by Ruben Mennes on 23/04/2020.
//

#ifndef LIBPROGRESSBAR_PROGRESSBAR_WIDGETS_AVERAGE_H_
#define LIBPROGRESSBAR_PROGRESSBAR_WIDGETS_AVERAGE_H_

#include "FixedLengthWidget.h"

namespace progressbar {
namespace widgets {

class AverageStepDuration: public FixedLengthWidget {
 public:
  AverageStepDuration();
  virtual ~AverageStepDuration() noexcept ;

  void stream_to(std::ostream &stream) const;
};

}
}

#endif //LIBPROGRESSBAR_PROGRESSBAR_WIDGETS_AVERAGE_H_
