//
// Created by Ruben Mennes on 21/04/2020.
//

#include "Text.h"

namespace progressbar{
namespace widgets{

Text::Text(std::string text):
FixedLengthWidget(), m_text(text) {

}

Text::~Text(){

}

void Text::stream_to(std::ostream &stream) const{
 stream << m_text;
}

Text& Text::set_text(std::string text) {
  m_text = text;
  return *this;
}

}
}
