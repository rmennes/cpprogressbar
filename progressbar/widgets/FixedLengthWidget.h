//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_FIXEDLENGTHWIDGET_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_FIXEDLENGTHWIDGET_H_

#include "AbstractWidget.h"

namespace progressbar {
namespace widgets {

class FixedLengthWidget : public AbstractWidget {
 public:
  FixedLengthWidget();
  virtual ~FixedLengthWidget();

  bool is_fixed() const {return true;}
  inline Maybe<Scale> get_variable_scale() const {return Maybe<Scale>();}
  void calculate_length(SymbolLength total, SymbolLength allocated, Scale scale_total);

};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_FIXEDLENGTHWIDGET_H_
