//
// Created by Ruben Mennes on 21/04/2020.
//

#include "AbstractWidget.h"
#include <cmath>

static unsigned int baseTwoDigits(unsigned long long x) {
  return x ? 64 - __builtin_clzll(x) : 0;
}

static unsigned int baseTenDigits(unsigned long long x) {
  static const unsigned char guess[65] = {
      0, 0, 0, 0, 1, 1, 1, 2, 2, 2,
      3, 3, 3, 3, 4, 4, 4, 5, 5, 5,
      6, 6, 6, 6, 7, 7, 7, 8, 8, 8,
      9, 9, 9, 9, 10, 10, 10, 11, 11, 11,
      12, 12, 12, 12, 13, 13, 13, 14, 14, 14,
      15, 15, 15, 15, 16, 16, 16, 17, 17, 17,
      18, 18, 18, 18, 19
  };
  static const unsigned int tenToThe[] = {
      1, 10, 100, 1000, 10000, 100000,
      1000000, 10000000, 100000000, 1000000000,
  };
  unsigned int digits = guess[baseTwoDigits(x)];
  return digits + (x >= tenToThe[digits]);
}

namespace progressbar{
namespace widgets {

AbstractWidget::AbstractWidget() : m_status(nullptr) {

}

AbstractWidget::~AbstractWidget() {

}

void AbstractWidget::calculate_length(SymbolLength, SymbolLength, Scale) {

}

void AbstractWidget::set_status(const ProgressStatus *status) {
  m_status = status;
}

SymbolLength AbstractWidget::length_unsigned(unsigned long long i) {
  return baseTenDigits(i);
}

}
}