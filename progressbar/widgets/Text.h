//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_TEXTWIDGET_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_TEXTWIDGET_H_

#include "FixedLengthWidget.h"
#include <string>

namespace progressbar {
namespace widgets {

class Text : public FixedLengthWidget {
 public:
  Text(std::string text = "");
  virtual ~Text();

  void stream_to(std::ostream &stream) const;

  Text &set_text(std::string text);

 protected:
  SymbolLength _calculate_size() const {return m_text.length();}
 private:
  std::string m_text;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_TEXTWIDGET_H_
