//
// Created by Ruben Mennes on 22/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_PERCENTAGE_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_PERCENTAGE_H_

#include "FixedLengthWidget.h"

namespace progressbar {
namespace widgets {

class Percentage: public FixedLengthWidget {
 public:
  Percentage(unsigned int precision_after_digits = 0);
  virtual ~Percentage();

  void stream_to(std::ostream& stream) const;

 private:
  unsigned int m_precision_after_digits;
};

}
}


#endif //LIBPROGRESSBAR_SRC_WIDGETS_PERCENTAGE_H_
