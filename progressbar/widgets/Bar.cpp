//
// Created by Ruben Mennes on 21/04/2020.
//

#include "Bar.h"

namespace progressbar{
namespace widgets{

Bar::Bar(Scale scale,
    char complete_char, char incompete_char):
VariableLengthWidget(scale),
m_complete_char(complete_char), m_incomplete_char(incompete_char),
m_rotating_step(0){

}

Bar::~Bar() noexcept {

}

void Bar::stream_to(std::ostream &stream, SymbolLength length) const {
  SymbolLength bar_length = length > 2 ? length - 2 : 0;

  stream << '[';

  Maybe<Ratio> ratio = m_status->ratio();
  if(!ratio && bar_length > 1) {
    for (SymbolLength i = 0; i < bar_length - 1; ++i) {
      stream << m_complete_char;
    }
    if (m_status->done()) {
      stream << m_complete_char;
    }else{
      stream << rotating_symbol();
    }
  }else{
    SymbolLength position = (double)ratio.value() * bar_length;
    for(SymbolLength i = 0 ; i < bar_length; ++i){
      if(i < position)stream << m_complete_char;
      else if(i == position)stream << rotating_symbol();
      else stream << m_incomplete_char;
    }
  }

  stream << ']';
}

Bar& Bar::set_complete_char(char complete_char) {
  m_complete_char = complete_char;
  return *this;
}

Bar& Bar::set_incomplete_char(char incomplete_char) {
  m_incomplete_char = incomplete_char;
  return *this;
}

char Bar::rotating_symbol() const {
  m_rotating_step = (m_rotating_step + 1) % m_rotating_symbol.size();
  return m_rotating_symbol[m_rotating_step];
}

const std::vector<char> Bar::m_rotating_symbol=
    {'/', '-', '\\', '|'};

}
}