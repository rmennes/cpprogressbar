//
// Created by Ruben Mennes on 21/04/2020.
//

#include "Steps.h"
#include <iomanip>

namespace progressbar {
namespace widgets {

Steps::Steps():
FixedLengthWidget(){

}

Steps::~Steps() noexcept {

}

void Steps::stream_to(std::ostream &stream) const {
  SymbolLength number_of_digits;
  Maybe<Step> max_steps = m_status->max_steps();
  if(max_steps){
    number_of_digits = length_unsigned(max_steps.value());
  }else{
    number_of_digits = std::min((unsigned int)5,
                                (unsigned int)length_unsigned(m_status->step()));
  }

  stream << std::setw(number_of_digits) << std::setfill(' ') << m_status->step();
  if(max_steps){
    Step max = max_steps.value();
    stream << '/' << std::setw(number_of_digits) << std::setfill(' ') << max;
  }

}

}
}
