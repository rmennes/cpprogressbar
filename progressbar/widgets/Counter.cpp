//
// Created by Ruben Mennes on 21/04/2020.
//

#include "Counter.h"
#include "../ProgressStatus.h"
#include <iomanip>

namespace progressbar {
namespace widgets {

Counter::Counter():
FixedLengthWidget(){

}

Counter::~Counter() noexcept {

}

void Counter::stream_to(std::ostream &stream) const {
  SymbolLength number_digits;
  if(m_status->max_steps()){
    number_digits = length_unsigned(m_status->max_steps().value());
  }else {
    number_digits = std::min((unsigned int)5,
                        (unsigned int)length_unsigned(m_status->step()));
  }

  stream << std::setw(number_digits) << std::setfill(' ') << m_status->step();
}

}
}