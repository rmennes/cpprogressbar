//
// Created by Ruben Mennes on 23/04/2020.
//

#include "SimpleETA.h"
#include <iomanip>

namespace progressbar{
namespace widgets{

SimpleETA::SimpleETA() {

}

SimpleETA::~SimpleETA(){

}

void SimpleETA::stream_to(std::ostream &stream) const {
  auto max_steps = m_status->max_steps();
  if(max_steps && m_status->step() > 0){
//    TimeDurationMs eta = (1.0-(double)ratio.value()) * (m_status->get_average_step_time() * m_status->max_steps().value());
      TimeDurationMs eta = (m_status->get_average_step_time())*(max_steps.value() - m_status->step());
      m_printer(eta, stream);
  }else{
    stream << "NAN";
  }
}

}
}