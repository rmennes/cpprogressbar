//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_ABSTRACTWIDGET_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_ABSTRACTWIDGET_H_

#include "../progressbar_types.h"
#include "../ProgressStatus.h"
#include <ostream>

namespace progressbar {
namespace widgets {

class AbstractWidget {
 public:
  AbstractWidget();
  virtual ~AbstractWidget();

  virtual bool is_fixed() const = 0;
  virtual Maybe<Scale> get_variable_scale() const = 0;
  virtual void stream_to(std::ostream &stream) const = 0;
  virtual void calculate_length(SymbolLength total, SymbolLength allocated, Scale scale_total);

  void set_status(const ProgressStatus* status);

 protected:
  static SymbolLength length_unsigned(unsigned long long);

  const ProgressStatus *m_status;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_ABSTRACTWIDGET_H_
