//
// Created by Ruben Mennes on 21/04/2020.
//

#include "FixedLengthWidget.h"

namespace progressbar{
namespace widgets {

FixedLengthWidget::FixedLengthWidget() :
    AbstractWidget() {
}

FixedLengthWidget::~FixedLengthWidget() noexcept {

}

void FixedLengthWidget::calculate_length(SymbolLength, SymbolLength, Scale) {

}


}
}