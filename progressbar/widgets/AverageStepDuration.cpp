//
// Created by Ruben Mennes on 23/04/2020.
//

#include "AverageStepDuration.h"
#include <iomanip>

namespace progressbar{
namespace widgets{

AverageStepDuration::AverageStepDuration() {

}

AverageStepDuration::~AverageStepDuration() noexcept {

}

void AverageStepDuration::stream_to(std::ostream &stream) const {
  double average_step_time = m_status->get_average_step_time();
  stream << std::fixed << std::setw(5) << std::setprecision(3) << std::setfill(' ')
  << average_step_time << "ms/step";
}


}

}