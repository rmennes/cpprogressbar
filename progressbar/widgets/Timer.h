//
// Created by Ruben Mennes on 22/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_TIMER_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_TIMER_H_

#include "FixedLengthWidget.h"
#include "../TimerPrinter.h"

namespace progressbar{
namespace widgets {

class Timer: public FixedLengthWidget {
 public:
  Timer();
  virtual ~Timer() noexcept ;

  void stream_to(std::ostream &stream) const;
 private:
  TimerPrinter m_printer;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_TIMER_H_
