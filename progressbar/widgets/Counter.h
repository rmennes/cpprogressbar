//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_WIDGETS_COUNTER_H_
#define LIBPROGRESSBAR_SRC_WIDGETS_COUNTER_H_

#include "FixedLengthWidget.h"

namespace progressbar {
namespace widgets {

class Counter: public FixedLengthWidget {
 public:
  Counter();
  virtual ~Counter() noexcept;

  void stream_to(std::ostream &stream) const;
};

}
}

#endif //LIBPROGRESSBAR_SRC_WIDGETS_COUNTER_H_
