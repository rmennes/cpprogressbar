//
// Created by Ruben Mennes on 22/04/2020.
//

#include "Percentage.h"
#include <iomanip>
#include <cfenv>

namespace progressbar{
namespace widgets{

Percentage::Percentage(unsigned int precision_after_digits):
FixedLengthWidget(), m_precision_after_digits(precision_after_digits){

}

Percentage::~Percentage() noexcept {

}

void Percentage::stream_to(std::ostream &stream) const {
  const Maybe<Ratio>& ratio = m_status->ratio();
  if(!ratio){
    stream << "NAN%";
  }else{
    int rounding = std::fegetround();
    std::fesetround(FE_DOWNWARD);
    double perc = ((double)ratio.value()) * 100;
    stream << std::fixed << std::setfill(' ') << std::setw(3)
            << std::setprecision(m_precision_after_digits) << perc << "%";
    SymbolLength l = 4 + m_precision_after_digits;
    if(m_precision_after_digits > 0){
      l += 1;
    }
    std::fesetround(rounding);
  }

}

}
}