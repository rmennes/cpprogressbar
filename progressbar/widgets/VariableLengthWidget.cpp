//
// Created by Ruben Mennes on 21/04/2020.
//

#include "VariableLengthWidget.h"
#include <cmath>

namespace progressbar{
namespace widgets {

VariableLengthWidget::VariableLengthWidget(Scale scale) :
    AbstractWidget(), m_scale(scale), m_symbols_to_use(10) {

}

VariableLengthWidget::~VariableLengthWidget() noexcept {

}

void VariableLengthWidget::calculate_length(SymbolLength total, SymbolLength allocated, Scale scale_total) {
  double symbols = total > allocated ? total - allocated : 0;
  symbols *= (scale_total > 0 ? m_scale / scale_total : 0);
//  symbols *= m_scale / scale_total;
  m_symbols_to_use = std::floor(symbols);
}

void VariableLengthWidget::stream_to(std::ostream &stream) const {
  stream_to(stream, get_symbols_to_use());
}

}
}