//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_INCLUDE_PROGRESSBAR_TYPES_H_
#define LIBPROGRESSBAR_SRC_INCLUDE_PROGRESSBAR_TYPES_H_

#include "Maybe.h"
#include <chrono>

namespace progressbar{

typedef unsigned int SymbolLength;
typedef Maybe<SymbolLength> WidgetLength;

typedef unsigned long long Step;
typedef double Scale;

typedef std::chrono::steady_clock::time_point TimePoint;
typedef unsigned long long TimeDurationMs;

}

#endif //LIBPROGRESSBAR_SRC_INCLUDE_PROGRESSBAR_TYPES_H_
