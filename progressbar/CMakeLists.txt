set(SRC
        ProgressBar.cpp
        ProgressStatus.cpp
        Ratio.cpp
        TimerPrinter.cpp
        WidgetFactory.cpp

        widgets/AbstractWidget.cpp
        widgets/AverageStepDuration.cpp
        widgets/Bar.cpp
        widgets/Counter.cpp
        widgets/FixedLengthWidget.cpp
        widgets/Percentage.cpp
        widgets/SimpleETA.cpp
        widgets/Steps.cpp
        widgets/Text.cpp
        widgets/Timer.cpp
        widgets/VariableLengthWidget.cpp
        )
add_library(progressbar ${SRC})
target_link_libraries(progressbar)
target_include_directories(progressbar INTERFACE ${CMAKE_CURRENT_SOURCE_DIR}/include)

install(TARGETS progressbar DESTINATION lib)
install(DIRECTORY . DESTINATION include/progressbar FILES_MATCHING PATTERN "*.h")
install(FILES ${PROJECT_BINARY_DIR}/config.h DESTINATION include/progressbar)