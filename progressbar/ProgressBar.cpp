//
// Created by Ruben Mennes on 21/04/2020.
//

#include "ProgressBar.h"
#include <sys/ioctl.h>
#include <unistd.h>
#include <sstream>


namespace progressbar{

ProgressBar::ProgressBar(WidgetFactory& widgets, std::ostream& stream): m_start(false), m_enabled(true),
m_stream(stream), m_status(), m_widgets(std::move(widgets.m_widgets)), m_total_scale(0), m_ms_delay(250),
m_last_ratio(0),
m_display_thread(), m_display_mutex(), m_display(false), m_display_run(false){
  for(AbstractWidgetPtr& widget: m_widgets){
    widget->set_status(&m_status);
    auto scale = widget->get_variable_scale();
    if(scale){
      m_total_scale += scale.value();
    }
  }
  start();
  widgets.m_widgets.clear();
}

ProgressBar::ProgressBar(Step end, WidgetFactory& widgets, std::ostream& stream):
m_start(false), m_enabled(true), m_stream(stream), m_status(end),
m_widgets(std::move(widgets.m_widgets)), m_total_scale(0), m_ms_delay(250),
m_last_ratio(0),
m_display_thread(), m_display_mutex(), m_display(false), m_display_run(false){
  for(AbstractWidgetPtr& widget: m_widgets){
    widget->set_status(&m_status);
    auto scale = widget->get_variable_scale();
    if(scale){
      m_total_scale += scale.value();
    }
  }
  start();
  widgets.m_widgets.clear();
}

ProgressBar::~ProgressBar() {
  stop();
}

void ProgressBar::push_back_widget(std::unique_ptr<widgets::AbstractWidget> &&widget) {
  std::lock_guard<std::mutex> lock(m_display_mutex);
  m_widgets.push_back(std::move(widget));
  m_total_scale += m_widgets.back()->get_variable_scale().value_or_default(0);
  m_widgets.back()->set_status(&m_status);
}

void ProgressBar::set_max_steps(Step max) {
  m_status.set_max(max);
}

void ProgressBar::enable() {
  m_enabled = true;
}

void ProgressBar::disable() {
  if(m_enabled.exchange(false)){
    m_stream << std::endl;
  }
}

void ProgressBar::start() {
  m_start = true;
  m_status.start();
  m_display = true;
  m_display_run = true;
  display();
  std::thread t1 = std::thread([this](){display_loop();});
  m_display_thread.swap(t1);
}

void ProgressBar::stop() {
  if(m_start) {
    m_display_run = false;
    m_display = false;
    m_display_cv.notify_all();
    m_display_thread.join();
    display();
    m_stream << std::endl;
    m_start = false;
  }

}

void ProgressBar::set_time_delay_ms(unsigned int time) {
  m_ms_delay = time;
}

ProgressBar& ProgressBar::operator+=(const Step & steps) {
  if(!m_start){
    start();
  }
  m_status += steps;
  m_display_cv.notify_all();
//  check_time_and_display();
  return *this;
}

ProgressBar& ProgressBar::operator++() {
  if(!m_start){
    start();
  }
  ++m_status;
  m_display_cv.notify_all();
//  check_time_and_display();

  return *this;
}

ProgressBar::operator bool() const {
  if(m_status.max_steps()){
    return m_status.step() < m_status.max_steps().value();
  }
  return false;
}

SymbolLength ProgressBar::get_window_size() const {
  struct winsize w;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
  return w.ws_col;
}

//void ProgressBar::check_time_and_display() {
//  auto now = std::chrono::steady_clock::now();
//  auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - m_last_display).count();
//  if(diff >= m_ms_delay){
//    display();
//  }
//}

void ProgressBar::display() {

  SymbolLength screen_width = get_window_size();

  SymbolLength fixed_length = 0;
  std::vector<DisplayElement> display_elements;
  for(AbstractWidgetPtr& widget: m_widgets){
    display_elements.emplace_back(widget);
    if(widget->is_fixed()){
      DisplayElement& de = display_elements.back();
      widget->stream_to(de.m_ss);
      de.m_set = true;
      fixed_length += de.size();
    }
  }

  for(DisplayElement& de: display_elements){
    if(de.m_set){
      m_stream << de.m_ss.str();
    }else{
      de.m_widget->calculate_length(screen_width, fixed_length, m_total_scale);
      de.m_widget->stream_to(m_stream);
    }
  }

  m_stream << "\r";
  m_stream.flush();
  m_last_ratio = m_status.ratio().value_or_default(0);
  m_last_display_time = std::chrono::steady_clock::now();
}

void ProgressBar::display_loop() {
  while(m_display_run){
    std::unique_lock<std::mutex> lock(m_display_mutex);
    if(m_display_cv.wait_for(lock, std::chrono::milliseconds(m_ms_delay), [this](){
      return std::chrono::duration_cast<std::chrono::milliseconds>(
          std::chrono::steady_clock::now() - m_last_display_time
          ).count() >= m_ms_delay;
    })){
      if(m_display){
        display();
      }
    }else{
      if(m_status.ratio() && (double)m_status.ratio().value() - (double)m_last_ratio >= 0.1){
        display();
      }
    }
  }
}

ProgressBar::DisplayElement::DisplayElement(const AbstractWidgetPtr & widget):
m_widget(widget), m_set(false), m_ss(){

}

}