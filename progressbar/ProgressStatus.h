//
// Created by Ruben Mennes on 21/04/2020.
//

#ifndef LIBPROGRESSBAR_SRC_PROGRESSSTATUS_H_
#define LIBPROGRESSBAR_SRC_PROGRESSSTATUS_H_

#include "progressbar_types.h"
#include "Ratio.h"
#include <chrono>

namespace progressbar {

class ProgressStatus {
 public:
  ProgressStatus();
  ProgressStatus(Step max);
  ProgressStatus(const ProgressStatus&);
  virtual ~ProgressStatus();

  ProgressStatus& operator=(const ProgressStatus&);

  inline const Maybe<Step>& max_steps() const {return m_max;}
  inline const Step& step() const {return m_step;}
  Maybe<Ratio> ratio() const;
  bool done() const {return m_end_time;};
  const TimePoint& get_start_time() const {return m_start_time;}
  const Maybe<TimePoint>& get_end_time() const {return m_end_time;}

  TimeDurationMs duration_ms() const;
  double get_average_step_time() const;

  ProgressStatus& operator+=(const Step&);
  ProgressStatus& operator++();
  ProgressStatus operator++(int);

  ProgressStatus& start();
  ProgressStatus& stop();

  inline bool is_started() const {return m_started;}
  inline bool is_ended() const {return m_end_time;}

  ProgressStatus& set_max();
  ProgressStatus& set_max(Step);
  ProgressStatus& set_max(Maybe<Step>);

  ProgressStatus& set_step(Step);

 private:
  void clear_cache();

  bool              m_started;
  Maybe<Step>       m_max;
  Step              m_step;
  TimePoint         m_start_time;
  Maybe<TimePoint>  m_end_time;

  mutable Maybe<double>   m_average_step_time;
};

}

#endif //LIBPROGRESSBAR_SRC_PROGRESSSTATUS_H_
