//
// Created by Ruben Mennes on 21/04/2020.
//

#include "ProgressBar.h"
#include "ProgressStatus.h"

namespace progressbar{

ProgressStatus::ProgressStatus(): m_started(false), m_max(), m_step(0),
m_start_time(std::chrono::steady_clock::now()),
m_end_time(), m_average_step_time(0){

}

ProgressStatus::ProgressStatus(Step max): m_max(max), m_step(0),
m_start_time(std::chrono::steady_clock::now()),
m_average_step_time(0){

}

ProgressStatus::ProgressStatus(const ProgressStatus &other):
m_max(other.m_max), m_step(other.m_step),
m_average_step_time(0){

}

ProgressStatus::~ProgressStatus() {

}

ProgressStatus& ProgressStatus::operator=(const ProgressStatus& other){
  m_max = other.m_max;
  m_step = other.m_step;
  return *this;
}

Maybe<Ratio> ProgressStatus::ratio() const{
  if(m_max){
    double ratio = (double)m_step/m_max.value();
    return Maybe<Ratio>(Ratio(ratio));
  }
  return Maybe<Ratio>();
}

TimeDurationMs ProgressStatus::duration_ms() const {
  TimePoint end_time = std::chrono::steady_clock::now();
  if(m_end_time){
    end_time = m_end_time.value();
  }
  return std::chrono::duration_cast<std::chrono::milliseconds>(
      end_time - m_start_time
      ).count();
}

double ProgressStatus::get_average_step_time() const {
  if(!m_average_step_time){
    m_average_step_time = (double)std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::steady_clock::now() - m_start_time
        ).count() / m_step;
  }
  return m_average_step_time.value();
}

ProgressStatus& ProgressStatus::operator+=(const Step & step) {
  m_step += step;
  clear_cache();
  return *this;
}

ProgressStatus& ProgressStatus::operator++(){
  ++m_step;
  clear_cache();
  return *this;
}

ProgressStatus ProgressStatus::operator++(int) {
  ProgressStatus result(*this);
  ++m_step;
  clear_cache();
  return result;
}

ProgressStatus& ProgressStatus::start() {
  m_started = true;
  m_start_time = std::chrono::steady_clock::now();
  return *this;
}

ProgressStatus& ProgressStatus::stop(){
  m_end_time = std::chrono::steady_clock::now();
  return *this;
}

ProgressStatus& ProgressStatus::set_max() {
  m_max.unset();
  return *this;
}

ProgressStatus& ProgressStatus::set_max(Step max){
  m_max = max;
  return *this;
}

ProgressStatus& ProgressStatus::set_max(Maybe<Step> max) {
  m_max = max;
  return *this;
}

ProgressStatus& ProgressStatus::set_step(Step step){
  m_step = step;
  return *this;
}

void ProgressStatus::clear_cache() {
  m_average_step_time.unset();
}

}