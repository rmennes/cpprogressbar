//
// Created by Ruben Mennes on 23/04/2020.
//

#include "WidgetFactory.h"

namespace progressbar{

void WidgetFactory::add_widget(AbstractWidgetPtr && widget) {
  m_widgets.push_back(std::move(widget));
}

}