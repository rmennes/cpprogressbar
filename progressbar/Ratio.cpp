//
// Created by Ruben Mennes on 21/04/2020.
//

#include "Ratio.h"
#include <algorithm>

namespace progressbar{

Ratio::Ratio(double p) : m_p(limit_value(p)){

}

Ratio Ratio::operator+(double p) const{
  return Ratio(m_p + p);
}

Ratio Ratio::operator+(const Ratio& r) const{
  return Ratio(m_p + r.m_p);
}

Ratio Ratio::operator-(double p) const {
  return Ratio(m_p - p);
}

Ratio Ratio::operator-(const Ratio& r) const {
  return Ratio(m_p - r.m_p);
}

Ratio Ratio::operator*(double p) const {
  return Ratio(m_p * p);
}

Ratio Ratio::operator*(const Ratio& r) const {
  return Ratio(m_p * r.m_p);
}

Ratio Ratio::operator/(double p) const {
  return Ratio(m_p / p);
}

Ratio Ratio::operator/(const Ratio& r) const {
  return Ratio(m_p / r.m_p);
}

Ratio& Ratio::operator+=(double p) {
  m_p = limit_value(m_p + p);
  return *this;
}

Ratio& Ratio::operator+=(const Ratio& r) {
  m_p = limit_value(m_p + r.m_p);
  return *this;
}

Ratio& Ratio::operator-=(double p) {
  m_p = limit_value(m_p - p);
  return *this;
}

Ratio& Ratio::operator-=(const Ratio& r) {
  m_p = limit_value(m_p - r.m_p);
  return *this;
}

Ratio& Ratio::operator*=(double p) {
  m_p = limit_value(m_p * p);
  return *this;
}

Ratio& Ratio::operator*=(const Ratio& r) {
  m_p = limit_value(m_p * r.m_p);
  return *this;
}

Ratio& Ratio::operator/=(double p) {
  m_p = limit_value(m_p / p);
  return *this;
}

Ratio& Ratio::operator/=(const Ratio& r) {
  m_p = limit_value(m_p / r.m_p);
  return *this;
}

std::ostream& operator<<(std::ostream& os, const Ratio& r){
  return os << r.m_p;
}

double Ratio::limit_value(double p) {
  return std::min(1.0, std::max(0.0, p));
}

}