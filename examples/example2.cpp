//
// Created by Ruben Mennes on 23/04/2020.
//

#include <iostream>
#include "progressbar/ProgressBar.h"
#include "progressbar/widgets/Bar.h"
#include "progressbar/widgets/Text.h"
#include "progressbar/widgets/Steps.h"
#include "progressbar/widgets/Timer.h"
#include "progressbar/widgets/SimpleETA.h"
#include "progressbar/widgets/AverageStepDuration.h"

#include <unistd.h>

using namespace progressbar;

int main() {
  std::cout << "Progressbar " << ProgressBar::version() << std::endl;

  WidgetFactory wfactory;
  wfactory.create_widget<widgets::Steps>();
  wfactory.create_widget<widgets::Text>(" ");
  wfactory.create_widget<widgets::Bar>();
  wfactory.create_widget<widgets::Text>(" ");
  wfactory.create_widget<widgets::Timer>();
  wfactory.create_widget<widgets::Text>(" ETA: ");
  wfactory.create_widget<widgets::SimpleETA>();
  wfactory.create_widget<widgets::Text>(" (");
  wfactory.create_widget<widgets::AverageStepDuration>();
  wfactory.create_widget<widgets::Text>(")");

  ProgressBar bar(wfactory);
  unsigned int steps = 800;
  unsigned int i = 0;
  bar.set_max_steps(steps);
  while(i < steps){
    usleep(1e5);
    ++bar;
    ++i;
  }
}
