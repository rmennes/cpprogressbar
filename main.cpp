#include <iostream>
#include "progressbar/ProgressBar.h"
#include "progressbar/widgets/Bar.h"
#include "progressbar/widgets/Text.h"
#include "progressbar/widgets/Steps.h"
#include "progressbar/widgets/Timer.h"
#include "progressbar/widgets/SimpleETA.h"
#include "progressbar/widgets/AverageStepDuration.h"

#include <unistd.h>

using namespace progressbar;

int main() {
  ProgressBar bar(800);
  bar.push_back_widget(std::make_unique<widgets::Steps>());
  bar.push_back_widget(std::make_unique<widgets::Text>(" "));
//  bar.push_back_widget(std::make_unique<widgets::Bar>());
//  bar.push_back_widget(std::make_unique<widgets::Text>("  "));
  bar.push_back_widget(std::make_unique<widgets::Bar>(2));
  bar.push_back_widget(std::make_unique<widgets::Text>(" "));
  bar.push_back_widget(std::make_unique<widgets::Timer>());
  bar.push_back_widget(std::make_unique<widgets::Text>(" ETA: "));
  bar.push_back_widget(std::make_unique<widgets::SimpleETA>());
  bar.push_back_widget(std::make_unique<widgets::Text>(" ("));
  bar.push_back_widget(std::make_unique<widgets::AverageStepDuration>());
  bar.push_back_widget(std::make_unique<widgets::Text>(")"));
  bar.start();
  for(unsigned int i = 0; i < 800; ++i){
    usleep(10000);
    ++bar;
  }

}
